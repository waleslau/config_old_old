### 需要安装的php模块：
```bash
yum install -y php-fpm php-pdo php-mysql php-gd  php
```

### Nginx 配置
- Nginx 配置文件位置：`/etc/nginx/nginx.conf`

因为未在配置文件内找到相关内容，所以需要我们手动添加

在server{}里添加下面内容
```bash
        location ~ \.php$ {
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            # 最佳实践里是 /usr/share/nginx/html 一样的效果
            fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include        fastcgi_params;
        }

```